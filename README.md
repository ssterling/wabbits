Wabbits
=======

A collection of fork bombs and other simple DoS programs in
various languages.

Run one of these and your computer should hang or at least lag;
a reboot or process kill should reset everything to normal.
However, there's still a 0.001% chance a measly fork bomb could
permanently damage your system; by running any of these, you
assuume the risk and acknowledge that I (the author) will not
be held liable for any damages.

They're fun to play with in a virtual machine, though.
Or an old, crappy computer you don't care to muck up.

Files requiring POSIX compatibility (UNIX, Linux, BSD, Mac I think)
are all in the [posix-unix/](posix-unix) directory; Windows-specifics
are likewise in [windows/](windows/).
POSIX files can be compiled on Windows with Cygwin or something of the like,
though.
